<?php include 'header.php';?>

<div id="topic_pass">
<ul>
<li><a href="/">HOME</a></li>
<li class="active">メニュー５</li>
</ul>
</div>
			<div class="main_wrapper">
				<div id="cntent">
					<h1 class="page_title">ご依頼者の声</h1>
					<p>「調べてよかった！」　依頼体験談・依頼者の声</p>
					<ul class="anchor mt_m">
						<li><a href="#sec_01">ご依頼者の声１</a></li>
						<li><a href="#sec_01_1">ご依頼者の声２</a></li>
						<li><a href="#sec_02">ご依頼者の声３</a></li>
					</ul>
					<section class="voices_sec border_sec_h" id="sec_01">
						<h2 class="middle_title">タイトル<span class="word_wrap">タイトルタイトルタイトル</span></h2>
						<p>説明・説明・説明・説明</p>
						<div class="inner">
							<div class="wrapper">
								<p>
									こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
								</p>
								<p class="mt">
								こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。<br>
								こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。<br>
								こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
								</p>
				
							</div>
						</div>
						<div class="staff">
							<div class="wrapper">
								<ul>
									<li>タイトルタイトルタイトルタイトル</li>
									<li class="mt">文章です。文章です。文章です。<span class="word_wrap">文章です。文章です。</span></li>
									<li class="mt">文章です。文章です。</li>
									<li class="mt">文章です。文章です。</li>
									<li class="mt">文章です。文章です。</li>
								</ul>
							</div>
						</div>
					</section>
					<section class="voices_sec man border_sec_h" id="sec_01_1">
						<h2 class="middle_title">タイトル<span class="word_wrap">タイトルタイトルタイトル</span></h2>
						<p>説明・説明・説明・説明</p>
						<div class="inner">
							<div class="wrapper">
								<p>
									こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
								</p>
								<p class="mt">
								こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。<br>
								こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。<br>
								こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
								</p>
				
							</div>
						</div>
						<div class="staff">
							<div class="wrapper">
								<ul>
									<li>タイトルタイトルタイトルタイトル</li>
									<li class="mt">文章です。文章です。文章です。<span class="word_wrap">文章です。文章です。</span></li>
									<li class="mt">文章です。文章です。</li>
									<li class="mt">文章です。文章です。</li>
									<li class="mt">文章です。文章です。</li>
								</ul>
							</div>
						</div>
					</section>
					<section class="voices_sec border_sec_h" id="sec_02">
						<h2 class="middle_title">タイトル<span class="word_wrap">タイトルタイトルタイトル</span></h2>
						<p>説明・説明・説明・説明</p>
						<div class="inner">
							<div class="wrapper">
								<p>
									こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
								</p>
								<p class="mt">
								こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。<br>
								こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。<br>
								こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
								</p>
				
							</div>
						</div>
						<div class="staff">
							<div class="wrapper">
								<ul>
									<li>タイトルタイトルタイトルタイトル</li>
									<li class="mt">文章です。文章です。文章です。<span class="word_wrap">文章です。文章です。</span></li>
									<li class="mt">文章です。文章です。</li>
									<li class="mt">文章です。文章です。</li>
									<li class="mt">文章です。文章です。</li>
								</ul>
							</div>
						</div>
					</section>
<?php include 'footer.php';?>