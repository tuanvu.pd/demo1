<?php include 'header.php';?>

<div id="topic_pass">
<ul>
<li><a href="/">HOME</a></li>
<li><a href="/">子メニュー</a></li>
<li class="active">タイトルタイトル</li>
</ul>
</div>
			<div class="main_wrapper">
				<div id="cntent">
					<h1 class="page_title">親タイトル親タイトル親タイトル親タイトル</h1>
					<div class="consulting_block bg pre-marriage">
						<p>
							タイトルタイトルタイトルタイトル
						</p>
						<dl>
							<dt class="phone_icon">電話での無料相談</dt>
							<dd><strong>００００－０００－０００</strong><span class="word_wrap">２４時間いつでも受付中</span></dd>
							<dt class="fax_icon">FAXでの無料相談</dt>
							<dd><strong>００００－０００－０００</strong><span class="word_wrap">２４時間いつでも受付中</span></dd>
							<dt class="form_icon">フォームでの無料相談</dt>
							<dd><a href=""><img src="img/btn_form_mail.png" alt="お問い合わせフォームはこちら"></a></dd>
						</dl>
					</div>
					<div class="sec white">
						<ul class="anchor">
							<li><a href="#sec_01">タイトルタイトルタイトルタイトル</a></li>
							<li><a href="#sec_02">タイトルタイトルタイトルタイトル</a></li>
							<li><a href="#sec_03">タイトルタイトルタイトルタイトル</a></li>
							<li><a href="#sec_04">タイトルタイトルタイトルタイトル</a></li>
						</ul>
					</div>
					<section class="sec white">
						<h2 class="middle_title">タイトルタイトルタイトルタイトル</h2>
						<p>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
						</p>
						<p class="mt">
						こちら文章です。こちら文章です。こちら文章です。
						</p>
					</section>

					<section class="sec white" id="sec_01">
						<h2 class="middle_title">タイトルタイトルタイトルタイトル</h2>
						<p>説明・説明・説明・説明説明・説明・説明・説明</p>
						<h3 class="low_title">説明・説明・説明・説明説明・説明・説明・説明説明・説明・説明・説明</h3>
						<ul class="disc">
							<li class="list">こちら文章です。こちら文章です。こちら文章です。</li>
							<li class="list">こちら文章です。こちら文章です。こちら文章です。</li>
							<li class="list">こちら文章です。こちら文章です。こちら文章です。</li>
							<li class="list">こちら文章です。こちら文章です。こちら文章です。</li>
							<li class="list">こちら文章です。こちら文章です。こちら文章です。</li>
							<li class="list">こちら文章です。こちら文章です。こちら文章です。</li>
							<li class="list">こちら文章です。こちら文章です。こちら文章です。</li>
							<li class="list">こちら文章です。こちら文章です。こちら文章です。</li>
							<li class="list">こちら文章です。こちら文章です。こちら文章です。</li>
						</ul>
					</section>

					<section class="sec white" id="sec_02">
					<h2 class="middle_title">タイトルタイトルタイトルタイトル</h2>
						<p>説明・説明・説明・説明説明・説明・説明・説明</p>
						<ul class="disc">
							<li class="list">こちら文章です。こちら文章です。</li>
							<li class="list">こちら文章です。こちら文章です。</li>
							<li class="list">こちら文章です。こちら文章です。</li>
						</ul>

						<table class="mq_table wide">
							<caption class="low_title">テーブルです。テーブルです。テーブルです。</caption>
							<thead>
								<tr>
									<th scope="col">列名</th>
									<th scope="col">列名</th>
									<th scope="col">列名</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scope="row" rowspan="3">タイトル</th>
									<td data-title="調査項目" class="mq">タイトル</td>
									<td data-title="着手金" class="mq">８０，０００円～</td>
								</tr>
								<tr>
									<td data-title="調査項目" class="mq">タイトル</td>
									<td data-title="着手金" class="mq">３５０，０００円</td>
								</tr>
								<tr>
									<td data-title="調査項目" class="mq">タイトル</td>
									<td data-title="着手金" class="mq">５００，０００円</td>
								</tr>
							</tbody>
						</table>

						<h3 class="low_title">タイトルタイトル</h3>
						<p class="ml">
							<strong>【タイトル】</strong><br>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。<br>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
						</p>
					</section>

					<section class="sec white" id="sec_03">
						<h2 class="middle_title">タイトルタイトルタイトルタイトル</h2>
						<p>
							<strong>【タイトルタイトル】</strong><br>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。<br>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
						</p>

						<p>
							<strong>【タイトルタイトル】</strong><br>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。<br>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
						</p>
						<p>
							<strong>【タイトルタイトル】</strong><br>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。<br>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
						</p>
						<p>
							<strong>【タイトルタイトル】</strong><br>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。<br>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
						</p>
						<div class="consulting_block">
							<dl>
								<dt class="phone_icon">電話での無料相談</dt>
								<dd><strong>００００－０００－０００</strong><span class="word_wrap">２４時間いつでも受付中</span></dd>
								<dt class="fax_icon">FAXでの無料相談</dt>
								<dd><strong>００００－０００－０００</strong><span class="word_wrap">２４時間いつでも受付中</span></dd>
								<dt class="form_icon">フォームでの無料相談</dt>
								<dd><a href=""><img src="img/btn_form_mail.png" alt="お問い合わせフォームはこちら"></a></dd>
							</dl>
						</div>
					</section>

					<section class="sec white" id="sec_04">
						<h2 class="middle_title">タイトルタイトルタイトルタイトル</h2>
						<h3 class="low_title chck">子タイトル子タイトル</h3>
						<div class="ml">
							<p>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。<br>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。<br>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
							</p>
							
						</div>
						<h3 class="low_title chck">子タイトル子タイトル</h3>
						<div class="ml">
							<p>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。<br>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。<br>
							こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。こちら文章です。
							</p>
							
						</div>
					</section>

					<?php include 'footer.php';?>