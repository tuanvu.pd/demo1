<?php include 'header.php';?>
<div id="topic_pass">
<ul>
<li><a href="/">HOME</a></li>
<li class="active">よくある質問</li>
</ul>
</div>
			<div class="main_wrapper">
				<div id="cntent">
					<h1 class="page_title">よくある質問</h1>
					<p class="mb">
						説明文章です。説明文章です。説明文章です。<br>
						説明文章です。説明文章です。説明文章です。説明文章です。説明文章です。
					</p>
					<ul class="anchor">
						<li><a href="#consulting">ご相談について</a></li>
						<li><a href="#contract">ご契約について</a></li>
						<li><a href="#investigation">調査について</a></li>
						<li><a href="#after">調査後について</a></li>
					</ul>

					<section class="border_sec_h faq" id="consulting">
						<h2 class="middle_title">ご相談について</h2>
						<div class="box_sec">
							<h3 class="inner_title"><span>Q</span>タイトルタイトルタイトルタイトル</h3>
							<div class="inner">
								<p class="b">
									文章です。文章です。文章です。文章です。
								</p>
								<p class="mt_m">
								文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
									<a href="" class="link_arrow">文章です。文章です。文章です。文章です。</a>
								</p>
							</div>
						</div>
						<div class="box_sec">
						<h3 class="inner_title"><span>Q</span>タイトルタイトルタイトルタイトル</h3>
							<div class="inner">
								<p class="b">
									文章です。文章です。文章です。文章です。
								</p>
								<p class="mt_m">
								文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
									<a href="" class="link_arrow">文章です。文章です。文章です。文章です。</a>
								</p>
							</div>
						</div>
						<div class="box_sec">
						<h3 class="inner_title"><span>Q</span>タイトルタイトルタイトルタイトル</h3>
							<div class="inner">
								<p class="b">
									文章です。文章です。文章です。文章です。
								</p>
								<p class="mt_m">
								文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
									<a href="" class="link_arrow">文章です。文章です。文章です。文章です。</a>
								</p>
							</div>
						</div>
					</section>
					<section class="border_sec_h faq" id="contract">
						<h2 class="middle_title">ご契約について</h2>
						<div class="box_sec">
						<h3 class="inner_title"><span>Q</span>タイトルタイトルタイトルタイトル</h3>
							<div class="inner">
								<p class="b">
									文章です。文章です。文章です。文章です。
								</p>
								<p class="mt_m">
								文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
									<a href="" class="link_arrow">文章です。文章です。文章です。文章です。</a>
								</p>
							</div>
						</div>
						<div class="box_sec">
							<h3 class="inner_title"><span>Q</span>タイトルタイトルタイトルタイトル</h3>
							<div class="inner">
								<p class="b">
									文章です。文章です。文章です。文章です。
								</p>
								<p class="mt_m">
								文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
									<a href="" class="link_arrow">文章です。文章です。文章です。文章です。</a>
								</p>
							</div>
						</div>
						<div class="box_sec">
							<h3 class="inner_title"><span>Q</span>タイトルタイトルタイトルタイトル</h3>
							<div class="inner">
								<p class="b">
									文章です。文章です。文章です。文章です。
								</p>
								<p class="mt_m">
								文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
									<a href="" class="link_arrow">文章です。文章です。文章です。文章です。</a>
								</p>
							</div>
						</div>
					</section>
					<section class="border_sec_h faq" id="investigation">
						<h2 class="middle_title">調査について</h2>
						<div class="box_sec">
						<h3 class="inner_title"><span>Q</span>タイトルタイトルタイトルタイトル</h3>
							<div class="inner">
								<p class="b">
									文章です。文章です。文章です。文章です。
								</p>
								<p class="mt_m">
								文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
									<a href="" class="link_arrow">文章です。文章です。文章です。文章です。</a>
								</p>
							</div>
						</div>
						<div class="box_sec">
						<h3 class="inner_title"><span>Q</span>タイトルタイトルタイトルタイトル</h3>
							<div class="inner">
								<p class="b">
									文章です。文章です。文章です。文章です。
								</p>
								<p class="mt_m">
								文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
									<a href="" class="link_arrow">文章です。文章です。文章です。文章です。</a>
								</p>
							</div>
						</div>
						<div class="box_sec">
						<h3 class="inner_title"><span>Q</span>タイトルタイトルタイトルタイトル</h3>
							<div class="inner">
								<p class="b">
									文章です。文章です。文章です。文章です。
								</p>
								<p class="mt_m">
								文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
									<a href="" class="link_arrow">文章です。文章です。文章です。文章です。</a>
								</p>
							</div>
						</div>
					</section>
					<section class="border_sec_h faq" id="after">
						<h2 class="middle_title">調査後について</h2>
						<div class="box_sec">
						<h3 class="inner_title"><span>Q</span>タイトルタイトルタイトルタイトル</h3>
							<div class="inner">
								<p class="b">
									文章です。文章です。文章です。文章です。
								</p>
								<p class="mt_m">
								文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
									<a href="" class="link_arrow">文章です。文章です。文章です。文章です。</a>
								</p>
							</div>
						</div>
						<div class="box_sec">
						<h3 class="inner_title"><span>Q</span>タイトルタイトルタイトルタイトル</h3>
							<div class="inner">
								<p class="b">
									文章です。文章です。文章です。文章です。
								</p>
								<p class="mt_m">
								文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
									<a href="" class="link_arrow">文章です。文章です。文章です。文章です。</a>
								</p>
							</div>
						</div>
						<div class="box_sec">
						<h3 class="inner_title"><span>Q</span>タイトルタイトルタイトルタイトル</h3>
							<div class="inner">
								<p class="b">
									文章です。文章です。文章です。文章です。
								</p>
								<p class="mt_m">
								文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
								文章です。文章です。文章です。文章です。文章です。文章です。文章です。文章です。<br>
									<a href="" class="link_arrow">文章です。文章です。文章です。文章です。</a>
								</p>
							</div>
						</div>
					</section>
					<?php include 'footer.php';?>

