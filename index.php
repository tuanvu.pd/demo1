<?php include 'header.php';?>

			<div id="visual">
				<div class="wrapper">
					<h2><img src="img/visual_catch.png" alt="不安を解消し、日常に平穏を取り戻しましょう。決して一人で悩まないで・・・" class="elm01"></h2>
				</div>
			</div>
			<section id="whats-hot-section">
				<div class="wrapper">
					<p class="arrow"><a href="whats-hot.html"><img src="img/arrow_right.gif" alt="新着情報" title="新着情報へ"></a></p>
					<ul class="article_slide">
						<!-- li><img src="/img/icon_new.gif" alt="NEW" class="icon"><a href="">新着情報の記事タイトルを1記事ずつスライドで表示</a></li>
						<li><a href="">次の記事</a></li -->
							<li><img src="img/icon_new.gif" alt="NEW" class="icon"><a href="">ニュース・ニュース・ニュース</a></li>

											<li><img src="img/icon_new.gif" alt="NEW" class="icon"><a href="">お知らせ・お知らせ・お知らせ</a></li>

											<li><img src="img/icon_new.gif" alt="NEW" class="icon"><a href="">ニュース・ニュース・ニュース</a></li>

											<li><img src="img/icon_new.gif" alt="NEW" class="icon"><a href="">お知らせ・お知らせ・お知らせ</a></li>

										
					</ul>
				</div>
			</section>
			<div class="main_wrapper">
				<div id="cntent">
					<section class="basic_principle">
						<h2 class="catchphrase">タイトルタイトルタイトル<strong class="word_wrap">タイトル</strong></h2>
						<p>
							こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
							こちら文章・こちら文章・こちら文章・こちら文章・こちら文章・こちら文章・こちら文章
						</p>
					</section>
					<section class="sec_promise">
						<h2 class="catchphrase">タイトルタイトルタイトル<strong class="word_wrap">タイトル</strong></h2>
						<dl>
							<dt><strong class="circle">No.<span>1</span></strong>子タイトル子タイトル子タイトル</dt>
							<dd>
							こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
							こちら文章・こちら文章・こちら文章・こちら文章・こちら文章・こちら文章・こちら文章
							</dd>
							<dt><strong class="circle">No.<span>2</span></strong>子タイトル子タイトル子タイトル子タイトル</dt>
							<dd>
							こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
							こちら文章・こちら文章・こちら文章・こちら文章・こちら文章・こちら文章・こちら文章
							</dd>
							<dt><strong class="circle">No.<span>3</span></strong>子タイトル子タイトル子タイトル<strong class="word_wrap">子タイトル</strong></dt>
							<dd>
							こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
							こちら文章・こちら文章・こちら文章・こちら文章・こちら文章・こちら文章・こちら文章
							</dd>
						</dl>
					</section>
					<div class="mq_show">
						<nav class="right_nav_services">
							<p class="sec_title">調査項目</p>
							<ul>
								<li><a href="services/infidelity.html" title="浮気調査・不倫調査・素行調査"><span><img src="img/right_nav/list_infidelity.png" alt="浮気調査・不倫調査・素行調査"></span></a></li>
								<li><a href="services/pre-marriage.html" title="結婚前調査・結婚信用調査"><span><img src="img/right_nav/list_pre-marriage.png" alt="結婚前調査・結婚信用調査"></span></a></li>
								<li><a href="services/pre-divorce.html" title="離婚前の慰謝料調査"><span><img src="img/right_nav/list_pre-divorce.png" alt="離婚前の慰謝料調査"></span></a></li>
								<li><a href="services/disappearance.html" title="行方失踪調査・家出調査・人探し"><span><img src="img/right_nav/list_disappearance.png" alt="行方失踪調査・家出調査・人探し"></span></a></li>
								<li><a href="services/credit.html" title="信用調査・身辺調査・自己信用調査"><span><img src="img/right_nav/list_credit.png" alt="信用調査・身辺調査・自己信用調査"></span></a></li>
								<li><a href="services/trouble-love.html" title="恋愛トラブル・男女問題相談室"><span><img src="img/right_nav/list_trouble-love.png" alt="恋愛トラブル・男女問題相談室"></span></a></li>
								<li><a href="services/itazura.html" title="嫌がらせ専門対策室"><span><img src="img/right_nav/list_itazura.png" alt="嫌がらせ専門対策室"></span></a></li>
								<li><a href="services/wiretap.html" title="盗聴盗撮発見調査・対策"><span><img src="img/right_nav/list_wiretap.png" alt="盗聴盗撮発見調査・対策"></span></a></li>
								<li><a href="services/stalker.html" title="ストーカー・つきまとい調査"><span><img src="img/right_nav/list_stalker.png" alt="ストーカー・つきまとい調査"></span></a></li>
								<li><a href="services/fraud.html" title="不正・詐欺実態調査"><span><img src="img/right_nav/list_fraud.png" alt="不正・詐欺実態調査"></span></a></li>
								<li><a href="services/overseas.html" title="海外調査・外国人調査"><span><img src="img/right_nav/list_overseas.png" alt="海外調査・外国人調査"></span></a></li>
								<li><a href="services/trouble.html" title="日常トラブル解決調査"><span><img src="img/right_nav/list_trouble.png" alt="日常トラブル解決調査"></span></a></li>
							</ul>
							<!-- p class="al_right"><a href="/services" class="link_arrow">一覧へ</a></p -->
						</nav>					<nav class="right_nav_map">
							<p class="sec_title">全国の相談窓口</p>
							<p>相談全部無料！お近くの窓口で、<span class="word_wrap">なんでもご相談くださいませ。</span></p>
							<ul>
								<li><a href="information/family_inv_office.html#tohoku" class="tohoku">東北地域</a></li>
								<li><a href="information/family_inv_office.html#chubu_tokay" class="chubu_tokay">中部・東海地域</a></li>
								<li><a href="information/family_inv_office.html#higashi_nihon" class="higashi_nihon">東日本地域</a></li>
								<li><a href="information/family_inv_office.html#nishi_nihon" class="nishi_nihon">西日本地域</a></li>
								<li><a href="information/family_inv_office.html#shikoku" class="shikoku">四国地域</a></li>
								<li><a href="information/family_inv_office.html#kyushu" class="kyushu">九州地域</a></li>
							</ul>
						</nav>
					</div>
					<section id="sec_voices">
						<h2 class="sec_title">ご依頼者の声</h2>
						<p class="link_page"><a href="" class="link_arrow">一覧を見る</a></p>
						<div class="wrapper clearfix slider">
							<div>
								<div class="sec">
									<article class="article woman">
										<div class="wrapper">
											<h3><strong>【タイトル】</strong></h3>
											<p>
											こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
											こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
											こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
											こちら文章・こちら文章・こちら文章・こちら文章・こちら文章
											</p>
										</div>
										<address class="address">神奈川県 Aさん</address>
									</article>
									<article class="article man">
										<div class="wrapper">
											<h3><strong>【タイトル】</strong></h3>
											<p>
											こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
											こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
											こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章
											</p>
										</div>
										<address class="address">京都府 Sさん</address>
									</article>
								</div>
							</div>
							<div class="sec">
								<article class="article woman">
									<div class="wrapper">
										<h3><strong>【タイトル】</strong></h3>
										<p>
										こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
											こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
											こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章
										</p>
									</div>
									<address class="address">東京都 Tさん</address>
								</article>
								<article class="article woman">
									<div class="wrapper">
										<h3><strong>【タイトル】</strong></h3>
										<p>
										こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
											こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
											こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章
										</p>
									</div>
									<address class="address">栃木県在住４８歳女性</address>
								</article>
							</div>
							<div class="sec">
								<article class="article woman">
									<div class="wrapper">
										<h3><strong>【タイトル】</strong></h3>
										<p>
										こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
											こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
											こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章
										</p>
									</div>
									<address class="address">富山県在住２６歳女性 Nさん</address>
								</article>
								<article class="article woman">
									<div class="wrapper">
										<h3><strong>【外国人調査】</strong></h3>
										<p>
										こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
											こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章<br>
											こちら文章・こちら文章・こちら文章・こちら文章・こちら文章こちら文章・こちら文章・こちら文章・こちら文章・こちら文章
										</p>
									</div>
									<address class="address">愛知県在住６２歳女性 Mさん</address>
								</article>
							</div>
						</div>
					</section>
					<section id="sec_news" class="clearfix">
						<h2 class="sec_title">新着情報配信中！</h2>
						<p><a href="" class="link_block">※ 新型コロナウイルス感染症への対応について ※</a></p>
					<section class="sec white">
						<h2 class="middle_title">相談ランキング</h2>
							<ul class="ranking">
								<li><strong>１位</strong><a href="">タイトル１タイトル１タイトル１タイトル１タイトル１</a></li>
								<li class="mt"><strong>２位</strong><a href="">タイトル2タイトル2タイトル2タイトル2タイトル2</a></li>
								<li class="mt"><strong>３位</strong><a href="">タイトル３タイトル３タイトル３タイトル３タイトル３タイトル３</a></li>
								<li class="mt"><strong>４位</strong><a href="">タイトル４タイトル４タイトル４タイトル４タイトル４</a></li>
								<li class="mt"><strong>５位</strong><a href="">タイトル５タイトル５タイトル５タイトル５タイトル５タイトル５タイトル５</a></li>
								<li class="mt"><strong>６位</strong><a href="">タイトル６タイトル６タイトル６タイトル６タイトル６タイトル６</a></li>
								<li class="mt"><strong>７位</strong><a href="">タイトル７タイトル７タイトル７タイトル７タイトル７タイトル７</a></li>
								<li class="mt"><strong>８位</strong><a href="">タイトル８タイトル８タイトル８タイトル８タイトル８タイトル８</a></li>
								<li class="mt"><strong>９位</strong><a href="">タイトル９タイトル９タイトル９タイトル９タイトル９タイトル９タイトル９</a></li>
								<li class="mt"><strong>１０位</strong><a href="">タイトル１０タイトル１０タイトル１０タイトル１０タイトル１０</a></li>
							</ul>
					</section>

						<div class="wrapper">
							<article class="inner">
								<h3 class="inner_title">お知らせ・調査報告</h3>
								<ul>
										<li><span class="icon"><img src="img/icon_article_info.png" alt="お知らせ"></span><a href="">タイトルタイトルタイトル</a></li>

											<li><span class="icon"><img src="img/icon_article_info.png" alt="お知らせ"></span><a href="">タイトルタイトルタイトルタイトルタイトルタイトルタイトル</a></li>

											<li><span class="icon"><img src="img/icon_article_info.png" alt="お知らせ"></span><a href="">タイトルタイトルタイトルタイトルタイトル</a></li>

											<li><span class="icon"><img src="img/icon_article_info.png" alt="お知らせ"></span><a href="">タイトルタイトルタイトル</a></li>

											<li><span class="icon"><img src="img/icon_article_info.png" alt="お知らせ"></span><a href="">タイトルタイトルタイトル</a></li>

											<li><span class="icon"><img src="img/icon_article_info.png" alt="お知らせ"></span><a href="">タイトルタイトルタイトル</a></li>

																		</ul>
								<p class="article_footer">
									<a href="whats-hot/info-list.html"><img src="img/btn_index.png" alt="一覧を見る"></a>
								</p>
							</article>
							<article class="inner">
								<h3 class="inner_title">実際の事例から見る相談ガイド</h3>
								<ul>
										<li><a href="" class="link_arrow">タイトルタイトルタイトルタイトルタイトルタイトル</a></li>

											<li><a href="" class="link_arrow">タイトルタイトルタイトルタイトル</a></li>

											<li><a href="" class="link_arrow">タイトルタイトルタイトルタイトルタイトルタイトル</a></li>

											<li><a href="" class="link_arrow">タイトルタイトルタイトルタイトル</a></li>

											<li><a href="" class="link_arrow">タイトルタイトルタイトルタイトル</a></li>

											<li><a href="" class="link_arrow">タイトルタイトルタイトルタイトルタイトルタイトル</a></li>

																		</ul>
								<p class="article_footer">
									<a href=""><img src="img/btn_index.png" alt="一覧を見る"></a>
								</p>
							</article>
						</div>
					</section>
					<?php include 'footer.php';?>