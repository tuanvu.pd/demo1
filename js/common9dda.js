$(document).ready(
	function(){

		$('#header .nav_mail .img_link').eq(0).attr('href', 'mailto:' + makeStr());

		$('#footer').before('<p class="pagetop"><a href="#"><img src="img/icon_pagetop.png" alt="ページトップへ"></a></p>');
		$(".pagetop").hide();
     // ↑ページトップボタンを非表示にする
	var scroll_point = $("#global_nav").offset().top;
	var header_height = $("#header").height();


    $(window).on("scroll", function() {
	//	console.log(scroll_point + "：" + header_height + "：" + $("#header").height());

		if ($(this).scrollTop() > scroll_point) {
			$("body").addClass("scroll");
			scroll_point = 96;
			$("body.scroll").css("padding-top", header_height);
		} else {
			$("body").removeClass("scroll");
			scroll_point = $("#global_nav").offset().top;
			$("body, body #header").removeAttr("style");
		}
		if ($(this).scrollTop() > 100) {
            // ↑ スクロール位置が100よりも小さい場合に以下の処理をする
            $('.pagetop').slideDown("fast");
            // ↑ (100より小さい時は)ページトップボタンをスライドダウン
        } else {
            $('.pagetop').slideUp("fast");
            // ↑ それ以外の場合の場合はスライドアップする。
        }
         
    // フッター固定する
 
        scrollHeight = $(document).height();
        // ドキュメントの高さ
        scrollPosition = $(window).height() + $(window).scrollTop();
        //　ウィンドウの高さ+スクロールした高さ→　現在のトップからの位置
        footHeight = 20;
        // フッターの高さ
		if ($(window).width() < 640) {
			footHeight =  0;
		}
                 
        if ( scrollHeight - scrollPosition  <= footHeight ) {
        // 現在の下から位置が、フッターの高さの位置にはいったら
        //  ".gotop"のpositionをabsoluteに変更し、フッターの高さの位置にする       
            $(".pagetop").css({
                "position":"absolute",
                "bottom": footHeight
            });
        } else {
        // それ以外の場合は元のcssスタイルを指定
            $(".pagetop").css({
                "position":"fixed",
                "bottom": "20px"
            });
        }

    });
 
   // #で始まるアンカーをクリックした場合に処理
		$('a[href^=#]').click(function() {
			if(typeof $(this).attr('onclick') === 'undefined' || $(this).attr('onclick') === false) {
				// スクロールの速度
				var speed = 400; // ミリ秒
				// アンカーの値取得
				var href= $(this).attr("href");
				// 移動先を取得
				var target = $(href == "#" || href == "" ? 'html' : href);
				// 移動先を数値で取得
				var position = target.offset().top - $("#header").height() - 32;
				// スムーススクロール
				$('body,html').animate({scrollTop:position}, speed, 'swing');
				return false;
			}
		});
		$("#mq_nav_btn").click(function() {
			menuShowHide("#global_nav", "#mq_nav_btn");
		});
	}
);

function aboutUs() {
	$('#mail').next().prepend('<a href="mailto:' + makeStr() + '">' + makeStr() + '</a>');
}

function openwin(url) {
 wn = window.open(url, 'win','width=520,height=500,status=no,location=no,scrollbars=yes,directories=no,menubar=no,resizable=no,toolbar=no');wn.focus();
}

function makeStr() {
	var str_b = 'acount2@';
	var str_h = 'acount1';
	var str_f = 'domain';

	str_h = str_h + str_b + str_f;

	return str_h;
}

function heightFunc(elm_left, elm_right ,elm_3, elm_4, elm_5) {
	var arr_height = [];
	for (i=0; i<arguments.length; i++) {
		arr_height.push($(arguments[i]).css('height').replace('px',''));
	}
	max_height = Math.max.apply(null,arr_height);

	for (i=0; i<arguments.length; i++) {
		$(arguments[i]).css('height', max_height);
	}
}

function menuShowHide(menu, start_elm) {
	if ($(start_elm).hasClass("close")) {
		$(menu).slideUp();
		$(start_elm).removeClass("close");
		$(start_elm + " span").text("MENU");
	} else {
		$(menu).slideDown();
		$(start_elm).addClass("close");
		$(start_elm + " span").text("CLOSE");
	}
}

// function visualFunc() {
		
// 		if ($(window).width() > 640) {
// 			$("#visual").append("<img src='img/preloader.gif' id='loader'>");
// 		}
// 		var img_array = [
// 			"img/visual_promise_first.png"
// 			,"img/visual_promise_second.png"
// 			,"img/visual_promise_third.png"
// 			,"img/visual_catch.png"
// 			,"img/visual_top_bg.gif"
// 			,"img/visual_top_main.png"
// 		];
// 	var num = 0;
// 	$.each(img_array,function(){
// 		var img = new Image();
// 		img.src = this;
// 		$(img).bind("load",function(){
// 			num++;
// 			if(num == img_array.length) {
// 				$("#visual #loader").remove();
// 				$("<div class='img_main'></div>").appendTo("#visual").fadeIn("slow", function() {
// 					$("<div class='bg_sky'></div>").appendTo("#visual").fadeIn("slow", function() {

// 						var slider_position = $("#visual .wrapper").width() * 0.5;
// 						var speed = 20500;
// 						slider();
// 						function slider() {
// 							$('#visual .bg_sky').stop().animate({
// 								backgroundPosition:"-" + (slider_position) + "px"
// 							}, speed, "swing");/*.animate({
// 								backgroundPosition:"0"
// 							}, speed, "swing");
// 							setTimeout(function() {
// 								slider()
// 							}, speed * 2);*/
// 						};
// 						$("#visual .elm01").show();
// 						setTimeout(function() {
// 							if ($(window).width() > 640) {
// 								$("#visual .elm01").css({"left": "29px", "opacity": "1"});
// 							}

// 							setTimeout(function() {
// 								$("#visual .elm01").css({"left": "0px", "opacity": "0"});
// 								setTimeout(function() {
// 									var promise_1 = $("<img src='img/visual_promise_first.png' alt='1. お客さまごとの専属担当制' class='promise_1'>");
// 									var promise_2 = $("<img src='img/visual_promise_second.png' alt='2. 納得のスピードと安心料金' class='promise_2'>");
// 									var promise_3 = $("<img src='img/visual_promise_third.png' alt='3. 一貫したサポート体制で未経験でも安心' class='promise_3'>");
// 									$("#visual .wrapper").append(promise_1);
// 									promise_1.ready(function() {
// 										promise_1.animate({
// 											"top": "10px",
// 											"opacity": "1"
// 										}, 1000, "swing", function() {
// 											$("#visual .wrapper").append(promise_2);
// 											promise_2.ready(function() {
// 												promise_2.animate({
// 													"top": "44px",
// 													"opacity": "1"
// 												}, 1000, "swing", function() {
// 													$("#visual .wrapper").append(promise_3);
// 													promise_3.ready(function() {
// 														promise_3.animate({
// 															"top": "86px",
// 															"opacity": "1"
// 														}, 1000, "swing");
// 													});
// 												});
// 											});
// 										});
// 									});

// 									setTimeout(function() {
// 									}, 1000);
// 								}, 1000);
// 							}, 4000);
// 						}, 1000);
// 					});
// 				});
// 			}
// 		});
// 	});

// 	var border_top = $("<div class='border_top'></div>");
// 	var border_right = $("<div class='border_right'></div>");
// 	var border_bottom = $("<div class='border_bottom'></div>");
// 	var border_left = $("<div class='border_left'></div>");
// 	var run_speed = 1000;
// 	$("body").append(border_top);
// 	border_top.ready(function() {
// 		border_top.animate({
// 			"width": "100%"
// 		}, run_speed, "linear", function() {
// 			$("body").append(border_right);
// 			border_right.ready(function() {
// 				border_right.animate({
// 					"height": "100%"
// 				}, run_speed, "linear", function() {
// 					$("body").append(border_bottom);
// 					border_bottom.ready(function() {
// 						border_bottom.animate({
// 							"width": "100%"
// 						}, run_speed, "linear", function() {
// 							$("body").append(border_left);
// 							border_left.ready(function() {
// 								border_left.animate({
// 									"height": "100%"
// 								}, run_speed, "linear");
// 							});
// 						});
// 					});
// 				});
// 			});
// 		});
// 	})
// }

function chk() {
	$(".chrctrs-chng").keyup(function(){
		var zenkigou = "＠－ー＋＿．，、";
        var hankigou = "@--+_...";
        var data = $(this).val();
        var str = "";
 
        // 指定された全角記号のみを半角に変換
        for (i=0; i<data.length; i++)
        {
            var dataChar = data.charAt(i);
            var dataNum = zenkigou.indexOf(dataChar,0);
            if (dataNum >= 0) dataChar = hankigou.charAt(dataNum);
            str += dataChar;
        }
        // 定番の、アルファベットと数字の変換処理
        var hankaku = str.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
		hankaku = hankaku.replace(/あ/g, 'a').replace(/い/g, 'i').replace(/う/g, 'u').replace(/え/g, 'e').replace(/お/g, 'o');
        $(this).val(hankaku);
	});

	$(".form .submit").click(function() {
		var alert_mes = "";
		$(".required_chk").removeClass("error");
		$(".form .required_chk").each(function() {
			$(this).removeClass("error");
			if($(this).val() == "") {
				alert_mes += "「" + $(this).attr("name") + "」\n";
				$(this).addClass("error");
			}
		});
		if(alert_mes == "") {
			$(".form").submit();
		} else {
			alert_mes = "以下の項目は必須入力項目です。\n\n" + alert_mes;
			alert(alert_mes);
		}
	});

	hideElmFunc("#select_time");
	$("#select_time").change(function() {
		hideElmFunc("#select_time");
	});
}

function hideElmFunc(elm) {
	$(".hide_elm").hide();
	var flg = $("option:selected", elm).attr("data-title");
	switch(flg) {
		case "mail":
			$(".hide_elm.mail").slideDown();
		break;

		case "tell":
			$(".hide_elm.tell").slideDown();
		break;
	}
}
