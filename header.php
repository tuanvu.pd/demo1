<!DOCTYPE html>
<html lang="ja-jp">
	
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>デモ用のサイト</title>
		<link rel="stylesheet" href="css/common46e5.css?ver=dNbWMNI">
		<link rel="stylesheet" href="css/mq00eb.css?ver=5BTx39N">
		<link href="https://fonts.googleapis.com/css?family=Coiny" rel="stylesheet">
		<script src="libs/1.8.3/jquery.min.js"></script>
		<script src="js/commonee41.js?ver=PNw0Jxu"></script>
		<!--[if lt IE 9]>
			<script  src="/js/html5shiv.min.js"></script>
			<link rel="stylesheet" type="text/css" href="css/ie.css" />
		<![endif]-->
		<script>
			$(function() {
				chk();
			});
		</script>



<link rel='dns-prefetch' href='http://s.w.org/' />
<script >
/* <![CDATA[ */
var wpp_params = {"sampling_active":"1","sampling_rate":"100","ajax_url":"https:\/\/www.fam-tantei.co.jp\/wp-json\/wordpress-popular-posts\/v1\/popular-posts","ID":"","token":"dac3caa50e","debug":""};
/* ]]> */
</script>
<script  src='wp-content/plugins/wordpress-popular-posts/assets/js/wpp-5.0.0.mince52.js?ver=5.0.2'></script>
<link rel='https://api.w.org/' href='wp-json/index.html' />
<link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embed7176.json?url=https%3A%2F%2Fwww.fam-tantei.co.jp%2F" />
<link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embed3465?url=https%3A%2F%2Fwww.fam-tantei.co.jp%2F&amp;format=xml" />
		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
			<style type="text/css" id="twentythirteen-header-css">
			.site-header {
			background: url(wp-content/themes/fam/images/headers/circle.png) no-repeat scroll top;
			background-size: 1600px auto;
		}
		@media (max-width: 767px) {
			.site-header {
				background-size: 768px auto;
			}
		}
		@media (max-width: 359px) {
			.site-header {
				background-size: 360px auto;
			}
		}
		</style>
	

		<script src="libs/slider/jquery.bxslider.min.js"></script>
		<link rel="stylesheet" href="libs/slider/jquery.bxslider.css">
		<script>
			$(function() {
				visualFunc();
				$('.slider').bxSlider({
					auto: true,
					infiniteLoop: true,
					speed: 1000,
					pause: 12000,
					touchEnabled: false,
				});
				$('.article_slide').bxSlider({
					auto: true,
					infiniteLoop: true,
					controls: false,
					pager: false,
					speed: 1000,
					pause: 8000
				});
			});
		</script>
	</head>
	<body class="home">
		<header id="header">
			<div class="wrapper" itemscope itemtype="https://schema.org/Book">
				<h1 id="site_name">タイトルタイトルタイトルタイトル</h1>
				<p id="logo"><a href="/"><img src="img/header/logo.png" alt="" itemprop="logo"></a></p>
				<button id="mq_nav_btn"><strong>メニューを開く</strong><span>MENU</span></button>
				<nav id="header_nav">
					<ul>
						<li><a href="" class="link_arrow mq_hide">サイトマップ</a></li>
						<li><a href="" class="link_arrow mq_hide">個人情報保護について</a></li>
						<li class="contact">
							<a href="" class="mq_show"><img src="img/header/img_dial.png" alt="電話番号はフリーダイヤル：0000-000-000 24時間受付"></a>
							<img src="img/header/img_dial.png" alt="電話番号はフリーダイヤル：0000-000-000 24時間受付" class="mq_hide"><br>
						</li>
					</ul>
				</nav>
				<p class="header_btn">
					<a href="" class="mq_hide"><img src="img/header/btn_form.png" alt="メール相談はこちら"></a>
					<a href="" class="mq_hide"><img src="img/header/btn_line.png" alt="LINEで気軽に無料相談"></a>
					<a href="" class="mq_show"><img src="img/header/btn_form_mq.png" alt="メール相談はこちら"></a>
					<a href="" class="mq_show"><img src="img/header/btn_line_mq.png" alt="LINEで気軽に無料相談"></a>
					<a href="https://twitter.com/" class="twitter" target="_blank" rel="nofollow"><img src="img/icon_twitter.png" alt="デモ用のサイト"></a>
				</p>
			</div>
			<nav id="global_nav">
				<ul>
					<li><a href="information.php" class="information">メニュー１</a></li>
					<li><a href="services.php" class="services">メニュー２</a></li>
					<li><a href="beginner.php" class="for-beginner">メニュー３</a></li>
					<li><a href="whats-hot.php" class="whats-hot">メニュー４</a></li>
					<li><a href="voices.php" class="voices">メニュー５</a></li>
					<li><a href="faq.php" class="faq">よくある質問</a></li>
				</ul>
			</nav>

		</header>
		<main role="main" id="main">