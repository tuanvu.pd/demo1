<section id="sec_form">
						<div class="head">
							<h2 class="inner_title">24時間無料相談お見積りフォーム</h2>
							<p>
								ご連絡をいただいたら24時間以内に返信いたします。<br>
								お急ぎの方は、こちらから折り返しお電話するので、お名前・電話番号の記入のみでけっこうです。
							</p>
						</div>
						<form class="form clearfix" method="post" action="https://www.fam-tantei.co.jp/consulting/form/mail_send">
							<h3 class="inner_title">以下のフォームから、新しい一歩を踏み出しましょう。</h3>
							<div class="wrapper">
								<p class="elm">
									<label for="お名前"><strong class="name">お名前</strong><small>（ニックネームでも可）</small><small class="required">*必須</small></label><br>
									<input id="お名前" type="text" name="お名前" value="" required placeholder="例：FAM 太郎" class="required_chk">
								</p>
								<p class="elm">
									<label for="お住まいの地域"><strong class="name">お住まいの地域</strong> <small class="required">*必須</small></label><br>
								<select id="お住まいの地域" name="お住まいの地域" required class="required_chk">
									<option value="" selected>選択してください。</option>
									<option value="海外">海外</option>
									<optgroup label="国内">
										<option value="北海道">北海道</option>
										<option value="青森県">青森県</option>
										<option value="岩手県">岩手県</option>
										<option value="宮城県">宮城県</option>
										<option value="秋田県">秋田県</option>
										<option value="山形県">山形県</option>
										<option value="福島県">福島県</option>
										<option value="茨城県">茨城県</option>
										<option value="栃木県">栃木県</option>
										<option value="群馬県">群馬県</option>
										<option value="埼玉県">埼玉県</option>
										<option value="千葉県">千葉県</option>
										<option value="東京都">東京都</option>
										<option value="神奈川県">神奈川県</option>
										<option value="新潟県">新潟県</option>
										<option value="富山県">富山県</option>
										<option value="石川県">石川県</option>
										<option value="福井県">福井県</option>
										<option value="山梨県">山梨県</option>
										<option value="長野県">長野県</option>
										<option value="岐阜県">岐阜県</option>
										<option value="静岡県">静岡県</option>
										<option value="愛知県">愛知県</option>
										<option value="三重県">三重県</option>
										<option value="滋賀県">滋賀県</option>
										<option value="京都府">京都府</option>
										<option value="大阪府">大阪府</option>
										<option value="兵庫県">兵庫県</option>
										<option value="奈良県">奈良県</option>
										<option value="和歌山県">和歌山県</option>
										<option value="鳥取県">鳥取県</option>
										<option value="島根県">島根県</option>
										<option value="岡山県">岡山県</option>
										<option value="広島県">広島県</option>
										<option value="山口県">山口県</option>
										<option value="徳島県">徳島県</option>
										<option value="香川県">香川県</option>
										<option value="愛媛県">愛媛県</option>
										<option value="高知県">高知県</option>
										<option value="福岡県">福岡県</option>
										<option value="佐賀県">佐賀県</option>
										<option value="長崎県">長崎県</option>
										<option value="熊本県">熊本県</option>
										<option value="大分県">大分県</option>
										<option value="宮崎県">宮崎県</option>
										<option value="鹿児島県">鹿児島県</option>
										<option value="沖縄県">沖縄県</option>
									</optgroup>
								</select>
								</p>
								<p class="elm">
									<label for="メールアドレス"><strong class="name">メールアドレス</strong><small>（半角入力）</small></label><br>
									<input id="メールアドレス" type="email" name="メールアドレス" placeholder="例：example@example.com" class="chrctrs-chng">
								</p>
								<p class="elm">
									<label for="電話番号"><strong class="name">電話番号</strong></label><br>
									<input id="電話番号" type="tel" name="電話番号"  placeholder="例：000-111-2222">
								</p>
							</div>
							<div class="wrapper">
								<p class="elm"><label for="ご相談内容"><strong class="name">ご相談内容</strong> <small class="required">*必須</small></label><br>
								<textarea id="ご相談内容" name="ご相談内容" class="required_chk"></textarea>
								</p>
								<p class="elm">
									<label for="ご希望の連絡方法"><strong class="name">ご希望の連絡方法</strong></label><br>
								<select id="ご希望の連絡方法" name="ご希望の連絡方法">
									<option value="メール" selected data-title="mail">メール</option>
									<option value="電話" data-title="tell">電話</option>
								</select>
								</p>
								<div class="hide_elm mail">
									<p>メールは24時間以内に返信します。</p>
								</div>
								<div class="hide_elm tell">
									<p>お電話でご連絡するご希望の時間がございましたら、以下からご指定いただけます。</p>
									<fieldset>
										<legend class="elm">
											<strong class="name">ご連絡希望の曜日</strong>
										</legend>
										<label class="word_wrap min"><input type="checkbox" name="ご連絡希望の曜日[]" value="日"> 日</label>
										<label class="word_wrap min"><input type="checkbox" name="ご連絡希望の曜日[]" value="月"> 月</label>
										<label class="word_wrap min"><input type="checkbox" name="ご連絡希望の曜日[]" value="火"> 火</label>
										<label class="word_wrap min"><input type="checkbox" name="ご連絡希望の曜日[]" value="水"> 水</label>
										<label class="word_wrap min"><input type="checkbox" name="ご連絡希望の曜日[]" value="木"> 木</label>
										<label class="word_wrap min"><input type="checkbox" name="ご連絡希望の曜日[]" value="金"> 金</label>
										<label class="word_wrap min"><input type="checkbox" name="ご連絡希望の曜日[]" value="土"> 土</label>
									</fieldset>
									<fieldset>
										<legend class="elm">
											<strong class="name">ご連絡可能な時間帯</strong>
										</legend>
										<label class="word_wrap"><input type="checkbox" name="ご連絡可能な時間帯[]" value="6時～9時"> 6時～9時</label>
										<label class="word_wrap"><input type="checkbox" name="ご連絡可能な時間帯[]" value="9時～12時"> 9時～12時</label>
										<label class="word_wrap"><input type="checkbox" name="ご連絡可能な時間帯[]" value="12時～15時"> 12時～15時</label>
										<label class="word_wrap"><input type="checkbox" name="ご連絡可能な時間帯[]" value="15時～18時"> 15時～18時</label>
										<label class="word_wrap"><input type="checkbox" name="ご連絡可能な時間帯[]" value="18時～21時"> 18時～21時</label>
										<label class="word_wrap"><input type="checkbox" name="ご連絡可能な時間帯[]" value="21時～24時"> 21時～24時</label>
										<label class="word_wrap"><input type="checkbox" name="ご連絡可能な時間帯[]" value="24時以降"> 24時以降</label>
									</fieldset>
								</div>
								<input type="hidden" name="token" value="krubatikaereba5rag5t7">
								<!-- p class="elm al_right">
									<label><strong class="name">希望予算</strong> <input name="希望予算" type="text" class="min"></label>
								</p -->
								<p class="elm al_right">
									<img src="img/btn_submit.png" alt="安心無料で相談する" class="submit">
								</p>
							</div>
						</form>
					</section>
				</div>
				<div id="right_nav">



					<nav class="right_nav_services">
						<p class="sec_title">項目一覧</p>
						<ul>
							<li><a href="/pre-marriage.php" title="浮気調査・不倫調査・素行調査">▶　項目１</a></li>
							<li><a href="/pre-marriage.php" title="浮気調査・不倫調査・素行調査">▶　項目２</a></li>
                            <li><a href="/pre-marriage.php" title="浮気調査・不倫調査・素行調査">▶　項目３</a></li>
                            <li><a href="/pre-marriage.php" title="浮気調査・不倫調査・素行調査">▶　項目４</a></li>
                            <li><a href="/pre-marriage.php" title="浮気調査・不倫調査・素行調査">▶　項目５</a></li>
                            
						</ul>
						<!-- p class="al_right"><a href="/services" class="link_arrow">一覧へ</a></p -->
					</nav>					<nav class="right_nav_map">
						<p class="sec_title">全国の相談窓口</p>
						<p>相談全部無料！お近くの窓口で、<span class="word_wrap">なんでもご相談くださいませ。</span></p>
						<ul>
							<li><a href="" class="tohoku">東北地域</a></li>
							<li><a href="" class="chubu_tokay">中部・東海地域</a></li>
							<li><a href="" class="higashi_nihon">東日本地域</a></li>
							<li><a href="" class="nishi_nihon">西日本地域</a></li>
							<li><a href="" class="shikoku">四国地域</a></li>
							<li><a href="" class="kyushu">九州地域</a></li>
						</ul>
					</nav>
					<p class="sec"><a href="/faq.php"><img src="img/right_nav/banner_faq.png" alt=""></a></p>
					<nav class="block">
						<p class="inner_title">メニュー１</p>
						<div class="wraper">
							<ul>
								<li><a href="" class="link_arrow">ご案内</a></li>
								<li><a href="" class="link_arrow">調査依頼の料金システム</a></li>
								<li><a href="" class="link_arrow">調査依頼の流れ</a></li>
							</ul>
						</div>
					</nav>
					<nav class="block consulting">
						<p class="inner_title">メニュー２</p>
						<div class="wraper">
							<ul>
								<li><a href="" class="link_arrow">無料電話相談窓口</a></li>
								<li><a href="" class="link_arrow">無料メール相談窓口</a></li>
								<li><a href="" class="link_arrow">面談相談予約窓口</a></li>
							</ul>
						</div>
					</nav>
				
					<div class="twitter block">
						<p class="inner_title">公式Twitter</p>
						<a class="twitter-timeline" href="https://twitter.com/" data-tweet-limit="3">Tweets by demo</a> <script async src="../platform.twitter.com/widgets.js"></script>
					</div>
				</div>
			</div>
			<section class="bnr_sec ver2">
				<h2 class="center_title">取扱分野別専門サイト</h2>
				<ul class="bnr_link">
					<li><a href="" target="_blank"><img src="img/bnr_stoker_ver2.jpg" alt=""><br class="mq_show">サイト閲覧・サイト閲覧</a></li>
					<li><a href="" target="_blank"><img src="img/bnr_hitosagashi_ver2.jpg" alt=""><br class="mq_show">サイト閲覧・サイト閲覧</a></li>
					<li><a href="" target="_blank"><img src="img/bnr_security_ver2.jpg" alt=""><br class="mq_show">サイト閲覧・サイト閲覧</a></li>
					<li><a href="" target="_blank"><img src="img/bnr_isyaryou.jpg" alt=""><br class="mq_show">サイト閲覧・サイト閲覧</a></li>
				</ul>
			</section>
		</main>
		<footer id="footer">
			<nav class="footer_nav wrapper">
				<h2 class="offscreen">Footer Navigation</h2>
				<dl>
					<dt class="mq_hide"><a href="" class="link_circle">メニュー１</a></dt>
					<dd class="mq_hide"><a href="" class="link_arrow_w">サブメニューサブメニュー</a></dd>
					<dd class="mq_hide"><a href="" class="link_arrow_w">サブメニューサブメニュー</a></dd>
					<dd class="mq_hide"><a href="" class="link_arrow_w">サブメニュー</a></dd>
					<dt><a href="" class="link_circle">メニュー２</a></dt>
					<dd><a href="" class="link_arrow_w">サブメニュー</a></dd>
					<dd><a href="" class="link_arrow_w">サブメニュー</a></dd>
					<dd><a href="" class="link_arrow_w">サブメニュー</a></dd>
				</dl>
				<dl class="clearfix mq_hide">
					<dt><a href="" class="link_circle">メニュー３</a></dt>
					<dd><a href="" class="link_arrow_w">サブメニュー</a></dd>
					<dd><a href="" class="link_arrow_w">サブメニュー</a></dd>
					<dd><a href="" class="link_arrow_w">サブメニュー</a></dd>
					<dd><a href="" class="link_arrow_w">サブメニューサブメニュー</a></dd>
					<dd><a href="" class="link_arrow_w">サブメニューサブメニュー</a></dd>
					<dd><a href="" class="link_arrow_w">サブメニュー</a></dd>
						
					<dd><a href="" class="link_arrow_w">サブメニュー</a></dd>
					<dd><a href="" class="link_arrow_w">サブメニュー</a></dd>
					<dd><a href="" class="link_arrow_w">サブメニュー</a></dd>
					<dd><a href="" class="link_arrow_w">サブメニュー</a></dd>
					<dd><a href="" class="link_arrow_w">サブメニュー</a></dd>
				</dl>
				<ul>
					<li><a href="" class="link_circle">新着情報</a></li>
					<li><a href="" class="link_circle">ご依頼者の声</a></li>
					<li><a href="" class="link_circle">よくある質問</a></li>
					<li><a href="" class="link_circle">相談窓口</a></li>
				</ul>
				<p class="banner"><a href="tel:00000000000" class="mq_show"><img src="img/banner_footer.png" alt="フリーダイヤル：0000-000-000 24時間いつでも受付 全国相談無料！"></a><img src="img/banner_footer.png" alt="フリーダイヤル：0000-000-000 24時間いつでも受付 全国相談無料！" class="mq_hide"></p>
			</nav>
			<div id="footer_copyright">
				<div class="wrapper">
					<ul>
						<li><a href="" class="link_arrow_w">HOME</a></li>
						<li><a href="" class="link_arrow_w">サイトマップ</a></li>
						<li><a href="" class="link_arrow_w">個人情報保護について</a></li>
						<li><a href="" class="link_arrow_w">スタッフ募集</a></li>
						<li><a href="" class="link_arrow_w">クレジットカード決済はこちら</a></li>
					</ul>
					<small>copyright デモ用のサイト</small>
				</div>
			</div>

        <script >
            (function(){
                document.addEventListener('DOMContentLoaded', function(){
                    let wpp_widgets = document.querySelectorAll('.popular-posts-sr');

                    if ( wpp_widgets ) {
                        for (let i = 0; i < wpp_widgets.length; i++) {
                            let wpp_widget = wpp_widgets[i];
                            WordPressPopularPosts.theme(wpp_widget);
                        }
                    }
                });
            })();
        </script>
        <link rel='stylesheet' id='wordpress-popular-posts-css-css'  href='wp-content/plugins/wordpress-popular-posts/assets/css/wppce52.css?ver=5.0.2' type='text/css' media='all' />
		</footer>
	</body>

<!-- Mirrored from www.fam-tantei.co.jp/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 31 May 2021 14:45:55 GMT -->
</html>
